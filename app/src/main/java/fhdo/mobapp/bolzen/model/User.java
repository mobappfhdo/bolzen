package fhdo.mobapp.bolzen.model;

import android.util.JsonReader;

import java.io.IOException;

public class User{
    public String getEmail() {
        return email;
    }

    public String getDisplayname() {
        return displayname;
    }

    private String email;
    private String displayname;
    private String authString;
    private Integer authProvider;
    public static final int AUTH_LOCAL_DEBUG = 0; // password saved in plain text

    public User(JsonReader reader) throws IOException {
        String field;
        reader.beginObject();
        while (reader.hasNext()){
            field = reader.nextName();
            switch(field){
                case "email":
                    email = reader.nextString();
                    break;
                case "displayname":
                    displayname = reader.nextString();
                    break;
                case "authString":
                    authString = reader.nextString();
                    break;
                case "authProvider":
                    authProvider = reader.nextInt();
                    break;
                default:
                    throw new IOException("JSON parse error: 'User' doesn't know '"+field+"'");
            }
        }
        reader.endObject();
    }
    public Boolean verifyAuth(String name, String pass){
        switch(authProvider){
            case AUTH_LOCAL_DEBUG:
                return (
                            name.equalsIgnoreCase(this.email)
                            ||name.equalsIgnoreCase(this.displayname)
                        ) && pass.equals(this.authString);
            default:
                return false;
        }
    }
}
