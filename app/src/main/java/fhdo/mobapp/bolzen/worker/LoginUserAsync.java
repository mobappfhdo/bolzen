package fhdo.mobapp.bolzen.worker;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStreamReader;

import fhdo.mobapp.bolzen.R;
import fhdo.mobapp.bolzen.model.User;


public class LoginUserAsync extends AsyncTask<String,Void,User> {
    private Activity context;

    public LoginUserAsync(Activity caller){
        context = caller;
    }
    @Override
    protected User doInBackground(String... params) {
        if (params.length==2){
            JsonReader reader = null;
            try {
                //connect(context.getString(R.string.database_server))
                reader = new JsonReader(new InputStreamReader(context.getResources().openRawResource(R.raw.fakeapi_users) , "UTF-8"));
                User u;
                reader.beginArray();
                while (reader.hasNext()) {
                    u = new User(reader);
                    if(u.verifyAuth(params[0],params[1])){
                        return u;
                    }
                }
                reader.endArray();
            } catch (IOException e) {
                Log.e("BolzenApp", "doInBackground: ", e);
                //e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (NullPointerException|IOException e) {
                    //e.printStackTrace();
                }
            }
        }
        return null;
    }
    protected void onPostExecute(User user) {
        if(user!=null){
            Toast.makeText(context.getApplicationContext(),
                    "Logged in as "+user.getDisplayname()+"("+user.getEmail()+")", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context.getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
        }
    }
}
