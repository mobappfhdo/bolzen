package fhdo.mobapp.bolzen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import fhdo.mobapp.bolzen.worker.LoginUserAsync;

public class MainActivity extends AppCompatActivity {

    private static final int CALLBACK_ID_PERMISSION_ACCESS_COARSE_LOCATION = 0x1001;
    private static final int CALLBACK_ID_PERMISSION_ACCESS_FINE_LOCATION = 0x1002;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void btnFindNearMeClicked(View view){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Toast.makeText(getApplicationContext(), "MISSING PERMISSION: LOCATION", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    CALLBACK_ID_PERMISSION_ACCESS_COARSE_LOCATION);

        }else{
            Toast.makeText(getApplicationContext(), "HAVE PERMISSION: LOCATION", Toast.LENGTH_SHORT).show();
            launchMapActivity();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALLBACK_ID_PERMISSION_ACCESS_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "PERMISSION GRANTED: LOCATION", Toast.LENGTH_SHORT).show();
                    launchMapActivity();
                } else {
                    Toast.makeText(getApplicationContext(), "PERMISSION DENIED: LOCATION", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void launchMapActivity(){
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
    public void btnLoginClicked(View view){
        String user = ((EditText)findViewById(R.id.editText)).getText().toString();
        String pass = ((EditText)findViewById(R.id.editText2)).getText().toString();
        new LoginUserAsync(this).execute(user,pass);
    }
    public void btnMapClicked(View view){
        launchMapActivity();
    }
    public void btnListClicked(View view){
        Toast.makeText(getApplicationContext(), "Soon.", Toast.LENGTH_SHORT).show();
    }
}
